package br.com.acesso.acesso.consumer;

import br.com.acesso.acesso.models.LogAcesso;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

@Component
public class LogAcessoConsumer {

    @KafkaListener(topics = "spec3-douglas-maldonado", groupId = "douglas-1")
    public void receber(@Payload LogAcesso logAcesso)
            throws CsvRequiredFieldEmptyException, IOException, CsvDataTypeMismatchException {
        System.out.println("Recebido Log");
        System.out.println("Cliente ID: " + logAcesso.getClienteId());
        System.out.println("Porta ID: " + logAcesso.getPortaId());
        System.out.println("Acesso: " + logAcesso.isAcesso());
        System.out.println("Data: " + logAcesso.getDataHoraAcesso());

        montaArquivoCSV(logAcesso);
    }

    public void montaArquivoCSV(LogAcesso logAcesso)
            throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        Writer writer = new FileWriter("/home/a2/aula-spring/api-acesso/LogAcesso.csv", true );
        StatefulBeanToCsv<LogAcesso> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(logAcesso);

        writer.flush();
        writer.close();
    }

}
